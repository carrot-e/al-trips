import { TestBed, inject } from '@angular/core/testing';

import { CanActivateDestinationService } from './can-activate-destination.service';

describe('CanActivateDestinationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanActivateDestinationService]
    });
  });

  it('should be created', inject([CanActivateDestinationService], (service: CanActivateDestinationService) => {
    expect(service).toBeTruthy();
  }));
});
