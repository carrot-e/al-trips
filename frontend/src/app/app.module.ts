import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TripComponent } from './trip/trip.component';
import { AlbumComponent } from './album/album.component';
import { ApiService } from './api.service';
import { CanActivateDestinationService } from './can-activate-destination.service';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MasonryModule } from 'angular2-masonry';
import { EverydayComponent } from './everyday/everyday.component';
import { LightboxComponent } from './lightbox/lightbox.component';

const appRoutes: Routes = [
    // {
    //     path: 'heroes',
    //     // component: HeroListComponent,
    //     data: { title: 'Heroes List' }
    // },
    {
        path: 'everyday',
        component: EverydayComponent
    },
    {
        path: ':destination/start',
        component: TripComponent,
        canActivate: [CanActivateDestinationService]
    },
    {
        path: ':destination/album',
        component: AlbumComponent,
        canActivate: [CanActivateDestinationService]
    },
    {
        path: '',
        component: HomeComponent,
        pathMatch: 'full'
    },
    // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        TripComponent,
        AlbumComponent,
        EverydayComponent,
        LightboxComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        InfiniteScrollModule,
        MasonryModule,
        RouterModule.forRoot(
          appRoutes,
          { enableTracing: true } // <-- debugging purposes only
      )
    ],
    entryComponents: [LightboxComponent],
    providers: [ApiService, CanActivateDestinationService],
    bootstrap: [AppComponent]
})
export class AppModule { }
