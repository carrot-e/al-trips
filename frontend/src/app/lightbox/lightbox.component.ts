import { Component, ComponentRef, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-lightbox',
    templateUrl: './lightbox.component.html',
    host: {
        '(document:keyup.esc)': 'destroy()',
        '(document:keyup)': 'onKeyup($event)'
    }
})
export class LightboxComponent implements OnInit {
    @Input() cmpRef: ComponentRef<LightboxComponent>;
    @Input() image: string;
    @Input() imagesConfig: {images: Array<string>};

    currentImageIndex: number;

    constructor() { }

    ngOnInit() {
        this.currentImageIndex = this.imagesConfig.images.indexOf(this.image);
    }

    onKeyup($event) {
        switch ($event.keyCode) {
            case 39: // arrow right
                this.next();
                break;
            case 37: // arrow left
                this.prev();
                break;
        }
    }

    destroy() {
        this.cmpRef.destroy()
    }

    next() {
        if (this.imagesConfig.images.length - this.currentImageIndex > 1) {
            this.currentImageIndex++;
        } else {
            this.currentImageIndex = 0;
        }
        this.image = this.imagesConfig.images[this.currentImageIndex];
    }

    prev() {
        if (this.currentImageIndex > 0) {
            this.currentImageIndex--;
        } else {
            this.currentImageIndex = this.imagesConfig.images.length - 1;
        }
        this.image = this.imagesConfig.images[this.currentImageIndex];
    }
}
