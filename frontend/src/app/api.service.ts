import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'
import { DomSanitizer } from "@angular/platform-browser";
import { environment } from '../environments/environment';

@Injectable()
export class ApiService {
    apiUrl: string = environment.apiUrl;
    config = {};

    constructor(
        private http: Http,
        private sanitizer: DomSanitizer) { }

    getConfig() {
        if (!Object.keys(this.config).length) {
            this.config = this.http.get(`${this.apiUrl}/config`)
                .toPromise()
                .then((res: Response) => res.json())
                .then(res => {
                    res.main.photo = this.sanitizer.bypassSecurityTrustStyle(`url(${res.main.photo})`);
                    return res;
                });
        }

        return new Promise(resolve => {
            resolve(this.config)
        });
    }

    getPhoto(destination) {
        return this.http.get(`${this.apiUrl}/${destination}/photo`)
            .toPromise()
            .then((res: Response) => res.json())
            .then((res) => {
                res.url = this.sanitizer.bypassSecurityTrustStyle(`url(${res.url})`);
                return res;
            });
    }

    getWeather(destination) {
        return this.http.get(`${this.apiUrl}/${destination}/weather`)
            .toPromise()
            .then((res: Response) => res.json())
    }

    getAlbum(destination) {
        return this.http.get(`${this.apiUrl}/${destination}/getAlbum`)
            .toPromise()
            .then((res: Response) => res.json())
    }

    getEveryday() {
        return this.http.get(`${this.apiUrl}/getEveryday`)
            .toPromise()
            .then((res: Response) => res.json())
    }
}
