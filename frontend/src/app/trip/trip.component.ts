import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../api.service";
import 'rxjs/add/operator/switchMap';
import {Observable} from "rxjs";


@Component({
    selector: 'app-trip',
    templateUrl: './trip.component.html'
})
export class TripComponent implements OnInit {
    destination = '';
    config: Promise<object>|object = {};
    destinationConfig: any;
    weather = {};
    photo = {};
    tripDates = {
        currentDate: null,
        daysPast: null,
        days: null,
        time: {
            hours: null,
            minutes: null,
            seconds: null
        }
    };
    countdown$: Observable<string>;

    constructor(
        private route: ActivatedRoute,
        private apiService: ApiService
    ) {
        this.config = this.apiService.getConfig();
    }

    ngOnInit() {
        this.route.paramMap
            .subscribe(d => {
                this.destination = d.get('destination');
                this.weather = this.apiService.getWeather(this.destination);
                this.photo = this.apiService.getPhoto(this.destination)
                    .then(photoData => {
                        photoData.color = this.getOverlayColor(photoData.color);
                        return photoData;
                    });

                this.apiService.getConfig()
                    .then(config => {
                        this.destinationConfig = Object.assign({}, config['destinations'][this.destination]);
                        this.destinationConfig.start = Date.parse(this.destinationConfig.start);
                        this.destinationConfig.end = Date.parse(this.destinationConfig.end);
                        this.destinationConfig.departureTime = Date.parse(this.destinationConfig.departureTime);

                        this.setDateValues();
                    });
            });
    }

    private setDateValues() {
        const tripConfig = this.destinationConfig;
        const currentDate = this.tripDates.currentDate = Date.now();
        const diff = tripConfig.departureTime - currentDate;
        const timePast = currentDate - tripConfig.end;

        this.tripDates.daysPast = Math.floor(timePast / 1000 / 60 / 60 / 24);
        this.tripDates.days = Math.floor(diff / 1000 / 60 / 60 / 24);
        this.tripDates.time.hours = Math.floor(diff / 1000 / 60 / 60 - this.tripDates.days * 24);
        this.tripDates.time.minutes = Math.floor(diff / 1000 / 60 - this.tripDates.days * 24 * 60 - this.tripDates.time.hours * 60);
        this.tripDates.time.seconds = Math.floor(diff / 1000 - this.tripDates.days * 24 * 60 * 60 - this.tripDates.time.hours * 60 * 60 - this.tripDates.time.minutes * 60);

        if (diff && timePast < 0) {
            this.runCountdown();
        }
    }

    private runCountdown() {
        this.countdown$ = Observable.interval(1000)
            .map(() => {
                return this.dhms(
                    Math.floor(
                        (new Date(this.destinationConfig.departureTime).getTime() - new Date().getTime())
                        / 1000
                    )
                )
            });
    }

    private dhms(t) {
        let days, hours, minutes, seconds;
        days = Math.floor(t / 86400);
        t -= days * 86400;
        hours = Math.floor(t / 3600) % 24;
        t -= hours * 3600;
        minutes = Math.floor(t / 60) % 60;
        t -= minutes * 60;
        seconds = t % 60;

        return `${days > 0 
                    ? (days === 1 ? days + ' day' : days + ' days')
                    : ''} 
                ${('0' + hours).slice(-2)}:${('0' + minutes).slice(-2)}:${('0' + seconds).slice(-2)}`;
    }

    private getOverlayColor(hex) {
        const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return 'rgba(' + [
                this.getAveragedColor(result[1]),
                this.getAveragedColor(result[2]),
                this.getAveragedColor(result[3]),
                0.8
            ].join(',') + ')';
    }

    private getAveragedColor(channel) {
        return Math.floor((parseInt('0x' + channel) + 153) / 2);
    }
}
