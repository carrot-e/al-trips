import { Component, ComponentFactoryResolver, HostListener, OnInit, ViewContainerRef } from '@angular/core';
import { ApiService } from "../api.service";
import { ActivatedRoute } from "@angular/router";
import { LightboxComponent } from "../lightbox/lightbox.component";

@Component({
    selector: 'app-album',
    templateUrl: './album.component.html'
})
export class AlbumComponent implements OnInit {

    album: any;
    displayedAlbum: Iterable<object>;
    destination: string;
    config: Promise<object>|object = {};
    currentPage: number = 0;
    limit: number = 10;
    isScrolling: boolean = false;
    showToTheTopButton: boolean = false;

    constructor(
        private apiService: ApiService,
        private route: ActivatedRoute,
        private componentFactoryResolver: ComponentFactoryResolver,
        private viewContainerRef: ViewContainerRef
    ) { }

    ngOnInit() {
        this.config = this.apiService.getConfig();
        this.route.paramMap
            .subscribe(d => {
                this.destination = d.get('destination');

                this.apiService.getAlbum(this.destination)
                    .then(album => {
                        album = album.album.reverse();
                        this.displayedAlbum = this.groupByDates(
                            [],
                            album.slice(this.currentPage++, this.limit)
                        );
                        this.album = album;

                        return album;
                    });
            });
    }

    // [{day: '', date: '', photos: []}]
    groupByDates(displayedAlbum, photos) {
        const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ];
        let photoDate = null;

        for (let photo of photos) {
            photoDate = new Date(Date.parse(photo.date) - 60 * 60 * 1000);

            if (!displayedAlbum.length || displayedAlbum[displayedAlbum.length - 1].day != photoDate.getDate()) {
                displayedAlbum.push({
                    day: photoDate.getDate(),
                    date: `${photoDate.getDate()} ${monthNames[photoDate.getMonth()]}`,
                    photos: []
                });
            }

            displayedAlbum[displayedAlbum.length - 1].photos.push(photo);
        }

        return displayedAlbum;
    }

    onScroll() {
        this.displayedAlbum = this.groupByDates(
            this.displayedAlbum,
            this.album.slice(
                this.currentPage * this.limit,
                ++this.currentPage * this.limit
            )
        );
    }

    @HostListener('window:scroll', [])
    onWindowScroll() {
        //update back to top visibility on scrolling
        if (!this.isScrolling) {
            this.isScrolling = true;
            window.requestAnimationFrame(() => this.checkBackToTop());
        }
    }

    openLightbox(image) {
        const factory = this.componentFactoryResolver.resolveComponentFactory(LightboxComponent);
        const ref = this.viewContainerRef.createComponent(factory);

        ref.instance.imagesConfig = {
            images: this.album.reduce((result, i) => {
                result.push(i.photo.url);
                return result;
            }, [])
        };
        ref.instance.cmpRef = ref;
        ref.instance.image = image;
        ref.changeDetectorRef.detectChanges();
    }

    checkBackToTop() {
        // browser window scroll (in pixels) after which "back to top" link is shown
        const offset = 300,
              windowTop = window.scrollY || document.documentElement.scrollTop;

        this.showToTheTopButton = windowTop > offset;
        this.isScrolling = false;
    }

    scrollToTop(event) {
        event.preventDefault();

        const start = window.scrollY || document.documentElement.scrollTop,
            duration = 700;
        let currentTime = null;

        const animateScroll = (timestamp) => {
            if (!currentTime) {
                currentTime = timestamp;
            }

            const progress = timestamp - currentTime,
                val = Math.max(this.easeInOutQuad(progress, start, -start, duration), 0);
            window.scrollTo(0, val);

            if(progress < duration) {
                window.requestAnimationFrame(animateScroll);
            }
        };

        window.requestAnimationFrame(animateScroll);
    }

    easeInOutQuad(t, b, c, d) {
        t /= d/2;
        if (t < 1) {
            return c/2*t*t + b;
        }
        t--;
        return -c/2 * (t*(t-2) - 1) + b;
    }
}
