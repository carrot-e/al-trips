import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { ApiService } from './api.service';

@Injectable()
export class CanActivateDestinationService implements CanActivate {

    constructor(private apiService: ApiService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot) {
        const destination = route.params.destination;
        return this.apiService.getConfig()
            .then(config => {
                let result = typeof config['destinations'][destination] !== 'undefined';

                if (!result) {
                    this.router.navigate(['/']);
                }
                return result;
            });
    }
}
