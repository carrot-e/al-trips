import { Component } from '@angular/core';
import { ApiService } from '../api.service';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
})
export class HomeComponent {
    config: Promise<object>|object = {};

    constructor(private apiService: ApiService) {
        this.config = this.apiService.getConfig()
            .then(config => this.prepareDestinations(Object.assign({}, config)));
    }

    private prepareDestinations(config) {
        let destinations = [];
        for (let d of Object.keys(config.destinations)) {
            destinations.push(config.destinations[d]);
        }
        config.destinations = destinations;

        return config
    }
}
