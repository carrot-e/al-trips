import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';
import { ApiService } from '../api.service';
import { LightboxComponent } from '../lightbox/lightbox.component';

@Component({
  selector: 'app-everyday',
  templateUrl: './everyday.component.html'
})
export class EverydayComponent implements OnInit {
    posts;
    displayedPosts;
    currentPage: number = 0;
    limit: number = 10;

  constructor(
      private apiService: ApiService,
      private componentFactoryResolver: ComponentFactoryResolver,
      private viewContainerRef: ViewContainerRef
  ) { }

  ngOnInit() {
    this.apiService.getEveryday()
        .then(posts => {
            posts.posts = posts.posts.map(p => {
                if (p.caption.indexOf('<div') !== 0) {
                    const options = {formatMatcher: 'basic', year: 'numeric', month: 'long', day: 'numeric'},
                        [title, body] = p.caption.split('\n\n'),
                        date = new Date(p.timestamp * 1000);

                    p.caption = `
                        <div class="title">${title.replace(/<p>|<\/p>/g, '')}</div>
                        <div class="date">${date.toLocaleString('ru-RU', options).replace('г.', '')}</div> 
                        <div class="body">${body}</div>`;
                }
                return p;
            });

            if (typeof posts.posts !== 'undefined') {
                this.displayedPosts = posts.posts.slice(this.currentPage++, this.limit);
            }
            this.posts = posts.posts;
        });
  }

    onScroll() {
        if (typeof this.posts !== 'undefined') {
            this.displayedPosts = this.posts.slice(0, ++this.currentPage * this.limit);
        }
    }

    openLightbox(image, post) {
        const factory = this.componentFactoryResolver.resolveComponentFactory(LightboxComponent);
        const ref = this.viewContainerRef.createComponent(factory);

        ref.instance.imagesConfig = {
            images: post.photos.reduce((result, i) => {
                result.push(i.original_size.url);
                return result;
            }, [])
        };
        ref.instance.cmpRef = ref;
        ref.instance.image = image;
        ref.changeDetectorRef.detectChanges();
    }
}
