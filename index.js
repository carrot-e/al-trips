const express = require('express'),
    app = express(),
    requestHandlers = require('./app/requestHandlers'),
    path = require('path'),
    config = require('./app/config');

app.use(express.static('frontend/dist'));

// Allow CORS only in DEV environment
if (!process.env.PORT) {
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    });
}

// ---- API part ----
app.get('/config', (req, res) => res.json(config));
app.get('/getEveryday', (req, res) => requestHandlers.getEverydayList(res));
app.get('/:destination/weather', (req, res) => requestHandlers.getWeather(req.params.destination, res));
app.get('/:destination/photo', (req, res) => requestHandlers.getPhoto(req.params.destination, res));
app.get('/:destination/getAlbum', (req, res) => requestHandlers.getAlbum(req.params.destination, res));

// ---- Non-API part ----
app.get('*', (req, res) => res.sendFile(path.join(__dirname + '/frontend/dist/index.html')));

app.listen(process.env.PORT || 8000, () => console.log('Server kek hyek at localhost:8000'));
