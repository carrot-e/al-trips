# Ilitka trip :sunglasses:
We don't travel a lot.  
Be convinced at https://arthur-lena.herokuapp.com

## How to run this locally:
Zero: You need `heroku` and `sass` installed locally.

First: setup dependencies
```
npm i
npm run setup

cd frontend
npm i
```

Second: start the app (from project root)
```
npm start
```

(Optional) install sass for the project
```
cd fronted
npm install sass
```

(Optional) compile css styles
```
./node_modules/.bin/sass src/assets/scss/main.scss src/assets/css/main.css
```

Third: start the frontend app
```
cd fronted
./node_modules/.bin/ng serve
```
And go to `localhost:4200`

## What if I want to deploy?
First you can build the Angular app
```
cd frontend
ng build --prod
```
Go to `localhost:8000` to check if everything is fine.

Note: When you are in master `pre-commit` hook will make sure Angular app is compiled and committed automatically.

Then in root directory run:
```
npm run deploy
```



