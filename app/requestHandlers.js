const uc = require('./api-clients/unsplashClient'),
    wc = require('./api-clients/weatherClient'),
    tc = require('./api-clients/tumblrClient'),
    config = require('./config');

const RequestHandlers = {
    getEverydayList(response) {
        new tc(config.everyday.tumblr)
            .get()
            .then(data => tc.asPosts(data))
            .then(posts => response.json({posts}));
    },

    getAlbum(destination, response) {
        new tc(config.destinations[destination].tumblr)
            .get()
            .then(data => tc.asAlbum(data))
            .then(album => response.json({album}));
    },

    getWeather(destination, response) {
        new wc(config.destinations[destination].weather)
            .get()
            .then(data => response.json(data));
    },

    getPhoto(destination, response) {
        new uc(config.destinations[destination].unsplash)
            .get()
            .then(data => response.json(data));
    }
};

module.exports = RequestHandlers;

