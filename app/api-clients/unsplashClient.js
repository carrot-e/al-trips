const request = require('request'),
    {promisify} = require('util'),
    requestAsync = promisify(request);

class UnsplashClient {
    constructor(query) {
        this.url = `http://api.unsplash.com/photos/random?query=${query}&orientation=landscape&client_id=b50aeb6316295c65611873caf9a6cc9b8e417108546143c54a8da40509e052dd`
    }

    get() {
        return requestAsync({
            url: this.url,
            json: true
        })
            .then(response => {
                if (response.statusCode === 200) {
                    return {
                        url: response.body.urls.regular,
                        color: response.body.color
                    }
                } else {
                    throw new Error('Unsplash API error happened')
                }
            })
            .catch(e => console.error(e));
    }
}

module.exports = UnsplashClient;
