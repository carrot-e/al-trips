const request = require('request'),
    {promisify} = require('util'),
    requestAsync = promisify(request);

/**
 * Gets data amount current weather in Dubai
 * @param {Object} response
 */
class WeatherClient {
    constructor(cityId) {
        this.url = `http://api.openweathermap.org/data/2.5/weather?id=${cityId}&units=metric&appid=fef1840fb736d063f652197db622d716`
    }

    // TODO add storing of response somewhere, to avoid using of API a lot
    get() {
        return requestAsync({
            url: this.url,
            json: true
        })
            .then(response => {
                if (response.statusCode === 200) {
                    let body = response.body,
                        utcSeconds = {
                        sunrise: body.sys.sunrise, // Sunrise time. unix, UTC
                        sunset: body.sys.sunset // Sunset time. unix, UTC
                    },
                    //var date1 = new Date(utcSeconds.sunrise * 1000); // Wed Oct 12 2016 05:16:13 GMT+0300 (EEST)
                        date1 = new Date((utcSeconds.sunrise * 1000) + (4 * 60 * 60 * 1000)),
                        timestr1 = date1.toLocaleTimeString(), // 5:16:13 AM
                    // var date2 = new Date(utcSeconds.sunset * 1000); local
                        date2 = new Date((utcSeconds.sunset * 1000) + (4 * 60 * 60 * 1000)), // UTC + 4
                        timestr2 = date2.toLocaleTimeString();

                    return {
                        city: body.name, // City name ---
                        country: body.sys.country, // Country code (GB, JP etc.) ---
                        //sunrise: body.sys.sunrise,
                        sunrise: timestr1, // ---
                        //sunset: body.sys.sunset,
                        sunset: timestr2, // ---
                        temperature: Math.round(body.main.temp), // Temperature, Celsius +++
                        pressure: body.main.pressure, // Atmospheric pressure, hPa ---
                        humidity: body.main.humidity, // Humidity, % +++
                        //windSpeed: body.wind.speed, // Wind speed, meter/sec +++
                        windSpeed: Math.round(body.wind.speed * 3.60), // Wind speed, m/sec +++, covert to km/h
                        windDirection: body.wind.deg, // Wind direction, degrees (meteorological) ---
                        cloudiness: body.clouds.all, // Cloudiness, % ---
                        // rain: body.rain + '.3h', // is not present in response :(
                        weatherMain: body.weather[0].main, // Group of weather parameters (Rain, Snow, Extreme etc.) ---
                        weatherDesc: body.weather[0].description, // Weather condition within the group +++
                        weatherIconUrl: 'http://openweathermap.org/img/w/' + body.weather[0].icon + '.png' // Weather icon id + url for this icon ---
                    };
                } else {
                    throw new Error('Weather API error happened');
                }
            })
            .catch(e => console.error(e))
    }
}

module.exports = WeatherClient;
