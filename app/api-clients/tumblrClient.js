const request = require('request'),
    {promisify} = require('util'),
    requestAsync = promisify(request);

class TumblrClient {
    constructor(tag) {
        this.limit = 20;
        this.data = [];
        this.totalPosts = null;
        this.currentOffset = 0;

        this.url = `http://api.tumblr.com/v2/blog/carrot-e.tumblr.com/posts/photo?tag=${tag}&api_key=pOuNn0JKDjhtJgIGNJknunovdQL8Eh6vH7IQVO7oratkGpMmbr`;
    }

    get() {
        return requestAsync({
            url: `${this.url}&offset=${this.currentOffset * this.limit}`,
            json: true
        })
            .then(response => {
                let body = response.body;

                if (response.statusCode === 200) {
                    this.data = this.data.concat(body);
                }

                if (this.totalPosts === null) {
                    this.totalPosts = body.response.total_posts;
                }

                this.currentOffset++;
                if (this.totalPosts > this.currentOffset * this.limit) {
                    return this.get();
                } else {
                    return this.data;
                }
            })
            .catch(e => console.error(e));
    }

    static asAlbum(data) {
        let album = [];

        for (let z = 0; z < data.length; z++) {
            if (data[z].meta.status === 200 && data[z].response.posts.length) {
                for (let i = 0; i < data[z].response.posts.length; i++) {
                    for (let j = 0; j < data[z].response.posts[i].photos.length; j++) {
                        album.push({
                            date: data[z].response.posts[i].date,
                            photo: data[z].response.posts[i].photos[j].original_size
                        });
                    }
                }
            }
        }

        return album;
    }

    static asPosts(data) {
        let posts = [];

        for (let z = 0; z < data.length; z++) {
            if (data[z].meta.status === 200 && data[z].response.posts.length) {
                for (let i = 0; i < data[z].response.posts.length; i++) {
                    posts.push({
                        caption: data[z].response.posts[i].caption,
                        photos: data[z].response.posts[i].photos,
                        timestamp: data[z].response.posts[i].timestamp
                    });
                }
            }
        }

        return posts;
    }
}

module.exports = TumblrClient;
