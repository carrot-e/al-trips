const config = {
    main: {
        title: '',
        photo: 'https://68.media.tumblr.com/d5a0f41a89c6b72ad37ea4fa3e919a3a/tumblr_osj4dhjgPQ1vihuygo1_1280.jpg',
        everydayPhoto: 'https://68.media.tumblr.com/5ab81550e53282bb0dd4f3288f3e81ad/tumblr_os1yfoJht71vihuygo1_1280.jpg',
    },
    everyday: {
        tumblr: 'al-everyday',
    },
    destinations: {
        dubai: {
            title: 'Dubai, Oct 2016',
            subtitle: 'are traveling to Dubai on 20-26 Oct 2016',
            photo: 'https://68.media.tumblr.com/3a253a588a3d0ce1f30917ba13950324/tumblr_os229wNl0v1vihuygo1_1280.jpg',
            destination: 'dubai',
            start: '2016-10-20',
            end: '2016-10-26',
            departureTime: '2016-10-20 01:45',
            tumblr: 'al-dubai2016',
            unsplash: 'dubai',
            weather: '292223'
        },
        sharm: {
            title: 'Sharm El Sheikh, Jan 2017',
            subtitle: 'are traveling to Sharm El Sheikh on 22-26 Jan 2017',
            photo: 'https://68.media.tumblr.com/bea1d9f3598d778c12cfe14dd15ff2c4/tumblr_os229wNl0v1vihuygo2_1280.jpg',
            destination: 'sharm',
            start: '2017-01-22',
            end: '2017-01-26',
            departureTime: '2017-01-22 04:50',
            tumblr: 'al-sharm2017',
            unsplash: 'egypt',
            weather: '349340'
        },
        larnaca: {
            title: 'Larnaca, Aug 2017',
            subtitle: 'are traveling to Larnaca on 19-24 Aug 2017',
            photo: 'https://68.media.tumblr.com/a76eefc0dfbc1efcac3fdbd37e40fb37/tumblr_ovea39IXwG1vihuygo1_1280.jpg',
            destination: 'larnaca',
            start: '2017-08-19',
            end: '2017-08-24',
            departureTime: '2017-08-19 08:05',
            tumblr: 'al-larnaca2017',
            unsplash: 'cyprus',
            weather: '146400'
        },
        barcelona: {
          title: 'Barcelona, March 2018',
          subtitle: 'are traveling to Barcelona on 17-21 March 2018',
          photo: 'https://66.media.tumblr.com/6926fdc1f5899de24e2f8de87522bf73/tumblr_ptk0e7iA9m1vihuygo1_1280.png',
          destination: 'barcelona',
          start: '2018-03-17',
          end: '2018-03-21',
          departureTime: '2018-03-17 15:50',
          tumblr: 'al-barcelona2018',
          unsplash: 'barcelona',
          weather: '3128760'
        },
        netherlands: {
            title: 'Netherlands, Sep 2019',
            subtitle: 'are traveling to Netherlands on 9-16 September 2019',
            photo: 'https://images.unsplash.com/photo-1515943138466-350cdce93c5d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1024&h=1024&fit=crop&ixid=eyJhcHBfaWQiOjF9',
            destination: 'netherlands',
            start: '2019-09-09',
            end: '2019-09-16',
            departureTime: '2019-09-09 07:00',
            tumblr: 'al-netherlands2019',
            unsplash: 'netherlands',
            weather: '2759794'
        }
    }
};

module.exports = config;
